'use strict';

var test = require('tape');
var request = require('supertest');
var app = require('../server');

test('Trae bien todos los postulantes', function (assert) {
  request(app)
    .get('/postulantes')
    .expect('Content-Type', /json/)
    .expect(200)
    .end(function (err, res) {
      assert.error(err, 'No error');
      assert.notEqual(res.body.length, 0, 'Listado mayor que 0');

      assert.end();
    });
});

test('Trae bien postulante id = 1', function (assert) {
  request(app)
    .get('/postulantes/1')
    .expect('Content-Type', /json/)
    .expect(200)
    .end(function (err, res) {

      var expectedPostulante = { id: 1, nombre: 'Kekin Soruco', imagen: 'kevin.jpg' };
      assert.error(err, 'No error');
      assert.same(res.body, expectedPostulante, 'Postulante id=1 ok');
      assert.end();
    });
});

test('insertar postulante', function (assert) {
  request(app)
    .post('/postulantes')
    .send({nombre:'test',imagen:'test.jpg'})
    .set('Accept', /application\/json/)
    .expect(200)
    .end(function (err, res) {

      console.log(err,res.body)
      assert.end();
    });
}); 