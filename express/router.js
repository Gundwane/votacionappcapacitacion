module.exports = (function () {
    'use strict';
    var router = require('express').Router();
    var postulanteModel = require('./model/postulante')
    //Postulantes
    router.get('/postulantes', (req, res) => {
        postulanteModel.getAll()
            .subscribe(data => {
                res.json(data);
            },
            err => {
                res.status(500).send(err)
            })
    });
    router.get('/postulantes/:id', (req, res) => {
        console.log(req.params);
        postulanteModel.getById(req.params.id)
            .subscribe(data => {
                res.json(data);
            },
            err => {
                res.status(500).send(err)
            })
    });
    router.post('/postulantes', (req, res) => {
        console.log(req.body);
        postulanteModel.insert(req.body)
            .subscribe(data => {
                res.json(data);
            },
            err => {
                res.status(500).send(err)
            })
    });
    router.patch('/postulantes/:id', (req, res) => {
        console.log(req.body);
        postulanteModel.update(req.params.id,req.body)
            .subscribe(data => {
                res.json(data);
            },
            err => {
                res.status(500).send(err)
            })
    });
    return router;
})();