'use strict';
var Rx = require('rxjs');

var db = require('../dbconnection');

var Postulante = {
    getAll: () => {
        return Rx.Observable.create(observer => {
            db.query('SELECT * FROM postulante', (err, results, fields) => {
                if (err) observer.error(err);
                observer.next(results)
            })
        })
    },
    getById: (id) => {
        return Rx.Observable.create(observer => {
            db.query('SELECT * FROM postulante WHERE id=?', [id], (err, results, fields) => {
                if (err) observer.error(err);
                observer.next(results[0] ? results[0] : {})
            })
        })
    },
    insert: (postulante) => {
        return Rx.Observable.create(observer => {
            db.query('INSERT INTO postulante (nombre,imagen) VALUES(?, ?);', [postulante.nombre, postulante.imagen], (err, result, fields) => {
                if (err) observer.error(err);
                postulante.id = result.insertId;
                observer.next(postulante)
            })
        })
    },
    update: (id, postulante) => {
        return Rx.Observable.create(observer => {
            db.query('UPDATE postulante SET nombre = ?, imagen=? WHERE id=?;', [postulante.nombre, postulante.imagen, id], (err, result, fields) => {
                if (err) observer.error(err);
                postulante.id = id;
                observer.next(result.changedRows > 0 ? postulante : {})
            })
        })
    }
}

module.exports = Postulante;