'use strict';
var express = require('express');
var app = express();

var bodyParser = require('body-parser')
app.use(bodyParser.json());

var router = require('./router')
app.use('/', router); 

app.listen(process.env.SERVER_PORT);
console.log(`Listening on port ${process.env.SERVER_PORT}...`);
module.exports = app; 
