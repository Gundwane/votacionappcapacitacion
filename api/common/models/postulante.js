'use strict';
var Rx = require('rxjs');
module.exports = function (Postulante) {

    // Para definir un método remoto hay que definirlo, en este caso se define en el código pero también puede hacerse en el archivo json de la entidad
    Postulante.remoteMethod('callbackHell', {
        accepts: { arg: 'postulanteId', type: 'number', required: true, description: 'Ingrese el id del postulante a buscar' },
        returns: { arg: 'result', type: 'object', root: true },
        http: { path: '/callbackHell', verb: 'get' }
    });
    Postulante.callbackHell = function (postulanteId, cb) { 
        console.log("Callback Hell");
        console.log("http://callbackhell.com/");
        console.log("------------------------");

        // Para acceder a otro modelo
        let Voto = Postulante.app.models.Voto;

        console.log("Comienza a buscar postulantes...");
        Postulante.findById(postulanteId, {}, function (err, postulante) {  // primer callback de la cadena de llamadas
            console.log("Retorna del findById de postulantes...", postulante, err);
            if (!postulante) {
                cb(new Error('No hay postulantes'));
            }
            console.log("Comienza a buscar votos...");
            Voto.find({ where: { postulanteId: postulanteId } }, function (err, votos) { // segundo callback de la cadena de llamadas
                console.log("Retorna del find de votos...", votos, err);
                let res = { postulanteEncontrado: postulante, votos: votos };
                cb(null, res);
            })
        })
    };

    // La definición del método remoto usePromises está en postulante.js (Se pude generar con "lb remote-method")
    Postulante.usePromises = (postulanteId, cb) => {
        console.log("Usando Promises");
        console.log("https://www.promisejs.org/");
        console.log("------------------------");

        // Para acceder a otro modelo
        let Voto = Postulante.app.models.Voto;

        let postulanteEncontrado = {};
        Postulante.findById(postulanteId)
            .then(postulante => {
                console.log("Retorna del findById de postulantes...", postulante);
                postulanteEncontrado = postulante;
                return Voto.find({ where: { postulanteId: postulante.id } })
            })
            .then(votos => {
                console.log("Retorna del find de votos...", votos);
                cb(null, postulanteEncontrado, votos)
            })
            .catch(err => {
                cb(new Error('Se produjo un error: ', err.message));
            })
    };

    Postulante.remoteMethod('useObservable', {
        accepts: { arg: 'postulanteId', type: 'number', required: true, description: 'Ingrese el id del postulante a buscar' },
        returns: [{ arg: 'postulante', type: 'object', root: false }, { arg: 'votos', type: 'object', root: false }],
        http: { path: '/useObservable', verb: 'get' }
    });
    Postulante.useObservable = (postulanteId, cb) => {
        console.log("Usando Observables");
        console.log("http://reactivex.io/rxjs/class/es6/Observable.js~Observable.html");
        console.log("------------------------");

        // Para acceder a otro modelo
        let Voto = Postulante.app.models.Voto;
        let postulanteEncontrado = {};
        Rx.Observable.fromPromise(Postulante.findById(postulanteId))
            .flatMap(postulante => {
                console.log("Retorna del findById de postulantes...", postulante);
                postulanteEncontrado = postulante;
                return Voto.find({ where: { id: postulante.id } })
            })
            .subscribe(votos => {
                console.log("Retorna del find de votos...", votos);
                cb(null, postulanteEncontrado, votos);
            },
            err => {
                cb(new Error('Se produjo un error: ', err.message));
            });
    };

    Postulante.remoteMethod('getPostulanteConVotos', {
        accepts: { arg: 'postulanteId', type: 'number', required: true, description: 'Ingrese el id del postulante a buscar' },
        returns: [{ arg: 'postulante', type: 'object', root: true }],
        http: { path: '/getPostulanteConVotos', verb: 'get' }
    });
    Postulante.getPostulanteConVotos = (postulanteId, cb) => {
        Rx.Observable.fromPromise(Postulante.findById(postulanteId, { include: 'votos' }))
            .subscribe(postulante => {
                console.log("Retorna del findById de postulantes...", postulante);
                cb(null, postulante);
            },
            err => {
                cb(new Error('Se produjo un error: ', err.message));
            })
    };

    Postulante.remoteMethod('replaceName', {
        accepts: [ 
            { arg: 'nombreBuscado', type: 'string', required: true, description: 'Nombre a buscar' },
            { arg: 'reemplazaPor', type: 'string', required: true, description: 'Reemplazar por' }
        ],
        returns: [{ arg: 'postulante', type: 'object', root: true }],
        http: { path: '/replaceName', verb: 'patch' }
    });
    Postulante.replaceName = (nombreBuscado, reemplazaPor, cb) => {
        Rx.Observable.fromPromise(Postulante.findOne({ where: { nombre: nombreBuscado } }))
            .filter(p => {
                if (!p) cb(new Error('No hay postulante con ese nombre'));
                return p
            })
            .flatMap(postulante => { 
                console.log("Retorna del findOne de postulantes...", postulante); 
                postulante.nombre = reemplazaPor
                return postulante.save();
            })
            .subscribe(postulanteActualizado => {
                cb(null, postulanteActualizado);
            },
            err => {
                cb(new Error('Se produjo un error: ', err.message));
            })
    };
};
