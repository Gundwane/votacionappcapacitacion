'use strict';
var Rx = require('rxjs');
const uuidv1 = require('uuid/v1');
module.exports = function (app) {
    Rx.Observable.fromPromise(app.models.Postulante.count()) 
    .filter(x=>x==0)
    .flatMap((cantidad)=>{
        console.log("Cantidad ",cantidad);
        let observables = [];
        observables.push(app.models.Postulante.create({nombre:'Prueba',imagen:'thumbnail-duckling-1.jpg'}));
        observables.push(app.models.Postulante.create({nombre:'Kekin Soruco',imagen:'thumbnail-kitten-1.jpg'}));
        observables.push(app.models.Postulante.create({nombre:'Ezequiel Morales',imagen:'thumbnail-puppy-1.jpg'}));
        observables.push(app.models.Postulante.create({nombre:'Pablo Pizarro',imagen:'thumbnail-duckling-2.jpg'}));
        observables.push(app.models.Postulante.create({nombre:'Joaquín Díaz',imagen:'thumbnail-kitten-2.jpg'}));
        observables.push(app.models.Postulante.create({nombre:'Celeste Zapata',imagen:'thumbnail-puppy-2.jpg'}));
        observables.push(app.models.Postulante.create({nombre:'Nahuel Arjona',imagen:'thumbnail-duckling-3.jpg'}));
        observables.push(app.models.Postulante.create({nombre:'Tomás Castillo',imagen:'thumbnail-kitten-3.jpg'}));
        observables.push(app.models.Postulante.create({nombre:'Nicolas Bianchetti',imagen:'thumbnail-puppy-3.jpg'}));
        observables.push(app.models.Postulante.create({nombre:'Enrique Campos',imagen:'thumbnail-duckling-4.jpg'}));
        observables.push(app.models.Postulante.create({nombre:'Marcelo Puebla',imagen:'thumbnail-kitten-4.jpg'}));
        observables.push(app.models.Postulante.create({nombre:'Leandro Ataguile',imagen:'thumbnail-puppy-4.jpg'}));
        return Rx.Observable.forkJoin(observables)
    })
    .flatMap((resultados)=>{
        let observables = [];
        observables.push(app.models.Voto.create({postulanteId:resultados[0].id,votanteId:uuidv1()})); 
        observables.push(app.models.Voto.create({postulanteId:resultados[0].id,votanteId:uuidv1()})); 
        observables.push(app.models.Voto.create({postulanteId:resultados[0].id,votanteId:uuidv1()})); 
        observables.push(app.models.Voto.create({postulanteId:resultados[1].id,votanteId:uuidv1()})); 
        observables.push(app.models.Voto.create({postulanteId:resultados[2].id,votanteId:uuidv1()})); 
        observables.push(app.models.Voto.create({postulanteId:resultados[3].id,votanteId:uuidv1()})); 
        return Rx.Observable.forkJoin(observables); 
    })
    .subscribe(resultados=>{
        console.log('Se crearon los votos ',resultados);
    })
}