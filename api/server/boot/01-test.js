'use strict';
var Rx = require('rxjs');
module.exports = function (app) {

    setTimeout(testAsyncPromisesObservables, 500);

    function testAsyncPromisesObservables() {
        console.log("\nPrestar atención al orden de TODAS las salidas en consola :)");


        console.log("\n1 Callback Hell");
        console.log("http://callbackhell.com/");
        console.log("------------------------");

        console.log("1.1 Comienza a buscar postulantes...");
        var postulanteId = 1;
        app.models.Postulante.findById(postulanteId, {}, function (err, postulante) {
            console.log("1.2 Vuelve del findById de postulantes...", postulante, err);
            if (!postulante) {
                throw new Error('No hay postulantes')
            }
            console.log("1.3 Comienza a buscar votos...");
            app.models.Voto.find({ where: { postulanteId: postulanteId } }, function (err, votos) {
                console.log("1.4 Vuelve del find de votos...", votos, err);
            })

        })
        console.log("1.5 Fin de bloque de callbacks");


        console.log("\n2 Usando Promises");
        console.log("https://www.promisejs.org/");
        console.log("------------------------");
        app.models.Postulante.findById(postulanteId)
            .then(postulante => {
                console.log("2.1 Vuelve del findById de postulantes...", postulante);
                return app.models.Voto.find({ where: { postulanteId: postulante.id } })
            })
            .then(votos => {
                console.log("2.2 Vuelve del find de votos...", votos);
            })
            .catch(err => {
                throw new Error('Se produjo un error: ', err.message)
            })

        console.log("\n3 Usando Observables");
        console.log("http://reactivex.io/rxjs/class/es6/Observable.js~Observable.html");
        console.log("------------------------");
        Rx.Observable.fromPromise(app.models.Postulante.findById(postulanteId))
            .flatMap(postulante => {
                console.log("3.1 Vuelve del findById de postulantes...", postulante);
                return app.models.Voto.find({ where: { id: postulante.id } })
            })

            .subscribe(votos => {
                console.log("3.2 Vuelve del find de votos...", votos);
            },
            err => {
                console.log("El error es ", err.message)
            });

        console.log("\n4 Como se hace lo anterior realmente en loopback");
        console.log("------------------------");
        Rx.Observable.fromPromise(app.models.Postulante.findById(postulanteId, { include: 'votos' }))
            .subscribe(postulante => {
                console.log("4.1 Vuelve del findById de postulantes...", postulante);
            },
            err => {
                console.log("El error es ", err.message)
            })
        console.log("\n4.2 Update en loopback");
        console.log("------------------------");
        Rx.Observable.fromPromise(app.models.Postulante.findOne({ where: { nombre: 'Prueba' } }))
            .filter(p=>p)
            .subscribe(postulante => {
                console.log("4.2.1 Vuelve del findOne de postulantes...", postulante); 
                postulante.nombre = 'José Luis Jofré';
                postulante.save();
            },
            err => {
                console.log("El error es ", err.message)
            })
        console.log("\n5 Usando Observables.forkJoin");
        console.log("------------------------");
        let obs = [];
        obs.push(app.models.Postulante.find({}))
        obs.push(app.models.Voto.find())
        Rx.Observable.forkJoin(obs)
            .subscribe(resultados => {
                console.log("5.1 En este punto me aseguro que se resolvieron los dos observables")
                console.log("Cantidad de postulantes= ", resultados[0].length);
                console.log("Cantidad de votos= ", resultados[1].length);

                //aca
            })
    }

};