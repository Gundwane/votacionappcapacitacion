import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { ServicesPage } from '../pages/services/services';
import { ContactPage } from '../pages/contact/contact';
import { BindingsPage } from '../pages/bindings/bindings';
import { TabsPage } from '../pages/tabs/tabs';
import { PostulantesPage } from '../pages/postulantes/postulantes';
import { MuestraVotosPage } from '../pages/muestra-votos/muestra-votos';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { PostulantesProvider } from '../providers/postulantes/postulantes';
import { HttpClientModule} from "@angular/common/http"
import { SDKBrowserModule } from '../providers/lbsdk/index';
import { TestConnectionProvider } from '../providers/test-connection/test-connection';

@NgModule({
  declarations: [
    MyApp,
    ServicesPage,
    ContactPage,
    BindingsPage,
    TabsPage,
    PostulantesPage,
    MuestraVotosPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    SDKBrowserModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ServicesPage,
    ContactPage,
    BindingsPage,
    TabsPage,
    PostulantesPage,
    MuestraVotosPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    PostulantesProvider,
    TestConnectionProvider
  ]
})
export class AppModule {}
