import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

/*
  Generated class for the PostulantesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PostulantesProvider {
  public postulantes: Array<any> = [];
  private serviceUrl = 'http://localhost:3001/api'
  constructor(public http: HttpClient) {
    console.log('Hello PostulantesProvider Provider');
  }

  getAll(): Observable<any> {
    return this.http.get(`${this.serviceUrl}/Postulantes`);
  }
}
