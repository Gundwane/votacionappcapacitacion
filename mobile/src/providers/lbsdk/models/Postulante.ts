/* tslint:disable */
import {
  Voto
} from '../index';

declare var Object: any;
export interface PostulanteInterface {
  "id"?: number;
  "nombre": string;
  "imagen"?: string;
  votos?: Voto[];
}

export class Postulante implements PostulanteInterface {
  "id": number;
  "nombre": string;
  "imagen": string;
  votos: Voto[];
  constructor(data?: PostulanteInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Postulante`.
   */
  public static getModelName() {
    return "Postulante";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Postulante for dynamic purposes.
  **/
  public static factory(data: PostulanteInterface): Postulante{
    return new Postulante(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Postulante',
      plural: 'Postulantes',
      path: 'Postulantes',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'number'
        },
        "nombre": {
          name: 'nombre',
          type: 'string'
        },
        "imagen": {
          name: 'imagen',
          type: 'string'
        },
      },
      relations: {
        votos: {
          name: 'votos',
          type: 'Voto[]',
          model: 'Voto',
          relationType: 'hasMany',
                  keyFrom: 'id',
          keyTo: 'postulanteId'
        },
      }
    }
  }
}
