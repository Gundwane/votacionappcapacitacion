/* tslint:disable */
import {
  Postulante
} from '../index';

declare var Object: any;
export interface VotoInterface {
  "id"?: number;
  "postulanteId": number;
  "fecha"?: Date;
  "votanteId": string;
  postulante?: Postulante;
}

export class Voto implements VotoInterface {
  "id": number;
  "postulanteId": number;
  "fecha": Date;
  "votanteId": string;
  postulante: Postulante;
  constructor(data?: VotoInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Voto`.
   */
  public static getModelName() {
    return "Voto";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Voto for dynamic purposes.
  **/
  public static factory(data: VotoInterface): Voto{
    return new Voto(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Voto',
      plural: 'Votos',
      path: 'Votos',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'number'
        },
        "postulanteId": {
          name: 'postulanteId',
          type: 'number'
        },
        "fecha": {
          name: 'fecha',
          type: 'Date'
        },
        "votanteId": {
          name: 'votanteId',
          type: 'string'
        },
      },
      relations: {
        postulante: {
          name: 'postulante',
          type: 'Postulante',
          model: 'Postulante',
          relationType: 'belongsTo',
                  keyFrom: 'postulanteId',
          keyTo: 'id'
        },
      }
    }
  }
}
