import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpClientModule } from "@angular/common/http";
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PostulanteApi } from '../../providers/lbsdk/index';
import 'rxjs/add/operator/map';


/*
  Generated class for the TestConnectionProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TestConnectionProvider {

  constructor(public http: HttpClient, public postulanteApi: PostulanteApi) {
    
  }

  getData(): Observable<Array<any>>{
    return this.http.get('http://localhost:3001/api/Postulantes')
    .map((res: Response) => res)
    .catch((error:any) => Observable.throw(error || 'Server error'));
  }

  postVoto(idVoto){
    let randomId = Math.random();
    let postData = {"postulanteId":idVoto, "votanteId":randomId};

    this.http.post('http://localhost:3001/api/Votos', postData)
     .map((res: Response) => res)
     .catch((error:any) => Observable.throw(error || 'Error en POST'))
     .subscribe(data =>{
       console.log(data);
     })
     
  }



  //https://randomuser.me/api/

}
