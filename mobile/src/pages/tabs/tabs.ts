import { Component } from '@angular/core';

import { PostulantesPage } from '../postulantes/postulantes';
import { MuestraVotosPage } from '../muestra-votos/muestra-votos';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = PostulantesPage;
  tab2Root = MuestraVotosPage;

  constructor() {

  }
}
