import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PostulantesPage } from './postulantes';

@NgModule({
  declarations: [
    PostulantesPage,
  ],
  imports: [
    IonicPageModule.forChild(PostulantesPage),
  ],
})
export class PostulantesPageModule {}
