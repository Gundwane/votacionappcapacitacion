import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TestConnectionProvider } from '../../providers/test-connection/test-connection';

/**
 * Generated class for the PostulantesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-postulantes',
  templateUrl: 'postulantes.html',
})
export class PostulantesPage {
  postulantes = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public testData: TestConnectionProvider) {
    
  }

  ionViewDidLoad(){
    this.loadData();
  }

  clicked(event){
    let idVoto = event.target.value;

    this.testData.postVoto(idVoto);
  }

  loadData(){
    this.testData.getData().subscribe(
      data => {this.postulantes = data},
      err => {
        console.log('Error!:', err)
      })
  }
}
