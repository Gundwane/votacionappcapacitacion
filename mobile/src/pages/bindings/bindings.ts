import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
//import { Device } from '@ionic-native/device';

@Component({
  selector: 'page-bindings',
  templateUrl: 'bindings.html',
  //providers: [Device]
})
export class BindingsPage {

  nombre: string;

  constructor(public navCtrl: NavController, /*private device: Device*/) {
    //this.nombre = this.device.uuid;
  }

  cambiarNombre() {
    this.nombre = 'Leandro';
    //this.nombre = this.device.uuid;
  }


}
