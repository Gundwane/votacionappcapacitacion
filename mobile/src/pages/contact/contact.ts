import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { PostulantesProvider } from '../../providers/postulantes/postulantes';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  constructor(public navCtrl: NavController, public postulanteService: PostulantesProvider) {

  }

}
