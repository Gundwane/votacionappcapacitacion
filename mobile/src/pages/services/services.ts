import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoopBackConfig,PostulanteApi, Postulante } from '../../providers/lbsdk/index'

@Component({
  selector: 'page-services',
  templateUrl: 'services.html',
  providers:[PostulanteApi]
})
export class ServicesPage {
  postulantes: Array<Postulante> = [];
  postulanteSeleccionado:Postulante = null;

  constructor(public navCtrl: NavController, public postulanteApi:PostulanteApi) {
    LoopBackConfig.setBaseURL('http://127.0.0.1:3001');
    LoopBackConfig.setApiVersion('api');

    this.cargarPostulantes()
  }
  cargarPostulantes() {
    this.postulanteApi.find({order:"id desc"})
    .subscribe((postulantes:Array<Postulante>)=>{
      this.postulantes = postulantes;
    })
  }
  seleccionaPostulante(postulante:Postulante){
    this.postulanteSeleccionado = postulante
  }

}
