import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MuestraVotosPage } from './muestra-votos';

@NgModule({
  declarations: [
    MuestraVotosPage,
  ],
  imports: [
    IonicPageModule.forChild(MuestraVotosPage),
  ],
})
export class MuestraVotosPageModule {}
