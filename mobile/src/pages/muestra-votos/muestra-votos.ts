import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TestConnectionProvider } from '../../providers/test-connection/test-connection';
import { LoopBackConfig,PostulanteApi, Postulante } from '../../providers/lbsdk/index'

/**
 * Generated class for the MuestraVotosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-muestra-votos',
  templateUrl: 'muestra-votos.html'
})
export class MuestraVotosPage {
  postulanteVotos = [];
  countedVotos = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,public provider: TestConnectionProvider, public postulanteApi: PostulanteApi) {
    LoopBackConfig.setBaseURL('http://127.0.0.1:3001');
    LoopBackConfig.setApiVersion('api');
  }

  ionViewDidLoad() {
    console.log(this.getCountVotos());
  }

  getPostulanteVotos(){
    this.postulanteApi.getPostulanteConVotos(1)
    .subscribe(data => {this.postulanteVotos.push(data)},
    err => {console.log('Error:', err)}
    )
  }

  getCountVotos(){
    this.postulanteApi.countVotos(1)
    .subscribe(data =>{this.countedVotos.push(data)},
    err => {console.log('Error', err)}
    )
  }

  //IDEA:Traer los votos ordenados por postulanteId (OrderBy?) y después contar las repeticiones.
  //Hacer un contador. Si el item se repite, sumar. Cuando deje de hacerlo, volver el contador a '1'
  
  // countVotos(data){
  //   let array = [];
  //   let count = 1, id;

  //   data.forEach(item => {
      
  //     console.log('Id 1:', item.postulanteId);
  //     console.log('Id antes:', id);
  //     if (id = item.postulanteId) {
  //       count++;
  //       array = [item.id, item.postulanteId, item.fecha, item.votanteId, count];
  //     }else{
  //       array = [item.id, item.postulanteId, item.fecha, item.votanteId, count];
  //       count = 1;
  //     }
  //     //console.log('Array inside function: ', array);
  //     id = item.postulanteId;
  //     console.log('Id despues:', id);
  //   });

  //   // data.map(function(index, value){
  //   //   array
  //   // })
  // }

}
