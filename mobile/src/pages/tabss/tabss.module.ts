import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabssPage } from './tabss';

@NgModule({
  declarations: [
    TabssPage,
  ],
  imports: [
    IonicPageModule.forChild(TabssPage),
  ],
})
export class TabssPageModule {}
