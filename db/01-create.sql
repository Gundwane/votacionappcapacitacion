CREATE DATABASE
IF NOT EXISTS `votacion-db`;
USE `votacion-db`;
CREATE TABLE `votacion-db`.`postulante`(
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR (45) NOT NULL,
  `imagen` VARCHAR (45) NULL,
  PRIMARY KEY(`id`));

CREATE TABLE `voto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `postulante_id` int(11) NOT NULL,
  `fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `votante_id` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `votante_id_UNIQUE` (`votante_id`),
  KEY `fk_voto_postulante_idx` (`postulante_id`),
  CONSTRAINT `fk_voto_postulante` FOREIGN KEY (`postulante_id`) REFERENCES `postulante` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
);
